﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ARController : MonoBehaviour {
    CharacController character;
    public GameObject Instruccions;
    public GameObject PausePanel;
    // Use this for initialization
    void Start () {
        ActiveInstruccionPanel(true);
    }	
	
    public void setCharacter(CharacController charac) {
        character = charac;
    }

    public CharacController GetCharacter() {
        return character;
    }
    public void  NextDialog() {
        if(GameController.active)
        character.NextDialog();
    }

    public void enfocar()
    {
        bool focusModeSet = CameraDevice.Instance.SetFocusMode(
    CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
        Debug.Log("<color=cyan>Focusing</color>");
        if (!focusModeSet)
        {
            Debug.Log("Failed to set focus mode (unsupported mode).");
        }
    }

    public void ActiveInstruccionPanel(bool val){
        Instruccions.SetActive(val);    
    }

    
    public void ActivePausePanel(bool val, float delay)
    {
        //PausePanel.SetActive(val);
        StartCoroutine(_ActivePausePanel(true, delay));
    }

    IEnumerator _ActivePausePanel(bool val,float delay) {
        yield return new WaitForSeconds(delay);
        PausePanel.SetActive(val);
    }
}
