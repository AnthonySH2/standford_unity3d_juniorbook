﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class CameraARFixed : MonoBehaviour {
	public Transform camera;
	public float t;
	// Use this for initialization
	void Start () {
		bool focusModeSet = CameraDevice.Instance.SetFocusMode(
			CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

		if (!focusModeSet)
		{
			Debug.Log("Failed to set focus mode (unsupported mode).");
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = Vector3.Lerp(this.transform.position,camera.position,t*Time.deltaTime);
		transform.rotation = Quaternion.Lerp(this.transform.rotation,camera.rotation,t*Time.deltaTime);
		this.GetComponent<Camera> ().fieldOfView = camera.Find("Camera").GetComponent<Camera> ().fieldOfView;
	}
}
