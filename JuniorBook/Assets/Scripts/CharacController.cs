﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.SceneManagement;

public class CharacController : MonoBehaviour {

	public enum state{Idle,	Enamorado,Enojado,Alegre}

	public string name;
	public GameObject Rocket;
	public AudioClip RocketSound;
	AudioSource audio = new AudioSource();
    public List<Texture> screens;
	public List<string> dialogs;
	public state estado;

	GameController game;
    Animator anim;
    bool isGame = false;	 
    bool canShowText=true;
    int indexDialog=0;

    // Use this for initialization
    void Start () {       
        audio = GetComponent<AudioSource>();        
        anim = GetComponent<Animator>();
       
		if (FindObjectOfType<GameController>())
        {
            game = FindObjectOfType<GameController>();
            isGame = true;
        }
        
    }
	
	// Update is called once per frame
	void Update () {
        if (!isGame)
            return;
		/*
        if (estado == state.idle && game.fruitsPickedInARow > 3)
        {
            setState(state.alegre);
            game.fruitsPickedInARow = 0;
        }
        if (estado == state.alegre && game.fruitsFallInARow > 3)
        {
            setState(state.idle);
            game.fruitsFallInARow = 0;
        }
        if (estado == state.idle && game.fruitsFallInARow > 3)
        {
            setState(state.enojado);
            game.fruitsFallInARow = 0;
        }
        if (estado == state.enojado && game.fruitsFallInARow > 5)
        {
            setState(state.alegre);
            game.fruitsFallInARow = 0;
        }*/
    }

    public void setState(string estate){
		anim.SetTrigger(estate);
    }
	public void setCharacterScreen(){

//		transform.Find ("Pantalla").gameObject.GetComponentInChildren<Renderer>().material.mainTexture = "";
	}
	public void turnCharacter(float X, float Y, float Z, float speedRotation){

		transform.DORotate (new Vector3 (X, Y, Z), speedRotation, RotateMode.LocalAxisAdd).SetDelay(2.5f).SetEase(Ease.InOutSine).OnComplete(()=>{return;});

	}
	public void jumpCharacter(float X, float Y, float Z, float jumpNumber, int jumpDuration ){
	
		transform.DOLocalJump(new Vector3 (X,Y,Z), jumpNumber, jumpDuration,1f, true).OnComplete(() =>{ return;});
	}

     public void StartedDialog() {
        indexDialog = 0;
        if (SceneManager.GetActiveScene().name == "_MainJunior")
        {
            setMessage(0, dialogs[indexDialog]);
        }
    }

    public void NextDialog() {
        if (SceneManager.GetActiveScene().name == "_MainJunior")
        {
            setMessage(1, dialogs[indexDialog]);
        }
    }

    public void setMessage(int Numdialog, string text) {
        if (GameController.active && canShowText)
        {
            canShowText = false;
            
            for (int i = 0; i < transform.Find("Dialog").childCount; i++)
            {
                transform.Find("Dialog").GetChild(i).gameObject.SetActive(false);
            }

            transform.Find("Dialog").GetChild(Numdialog).gameObject.SetActive(true);
            StartCoroutine(LetterByLetter(transform.Find("Dialog").transform.GetChild(Numdialog).GetChild(0).GetComponent<TextMeshProUGUI>(), text, 0.1f));            
            indexDialog++;
            
        }
    }

    public void DesactivarDialogos() {
        for (int i = 0; i < transform.Find("Dialog").childCount; i++)
        {
            transform.Find("Dialog").GetChild(i).gameObject.SetActive(false);
        }

    }

    public void StartAnim() {
        setState(Constants.IDLE);
    }
    public void Alegrar()
    {
		setState(Constants.ALEGRE);
    }

    IEnumerator LetterByLetter(TextMeshProUGUI mesh,string text,float delay) {
        
        string[] saltoLinea= text.Split('-');
        mesh.text = "";        
        for (int j = 0; j < saltoLinea.Length; j++)
        {
        char[] AnimateText = saltoLinea[j].ToCharArray();
        
        for (int i = 0; i < AnimateText.Length; i++)
        {
            mesh.text += AnimateText[i].ToString();
            yield return new WaitForSeconds(delay);
        }
            mesh.text += '\n'.ToString();
            /*mesh.richText = false;
            mesh.richText = true;*/
        }
        if (indexDialog == dialogs.Count)
        {
            indexDialog = 0;
            canShowText = false;
            FindObjectOfType<ARController>().GetCharacter().Alegrar();
            FindObjectOfType<ARController>().ActivePausePanel(true, 2.5f);
        }
        else
        {
            canShowText = true;
        }
    }
    public void SetScreen(string name)
    {
        Texture choose = screens[0];

        foreach (var item in screens)
        {
            if (item.name == name)
            {
                choose = item;
                break;
            }
        }
        GameObject.Find("Pantalla").GetComponent<MeshRenderer>().material.mainTexture = choose;
    }

    public void DispararCohete(Vector3 point) {

            GameObject cohe = Instantiate(Rocket, GameObject.Find("DisparadorCohete").transform.position, Rocket.transform.rotation) as GameObject;
			//cohe.transform.DOMove(point, 1f, false);        
			Vector3 direcc = (point - cohe.transform.position).normalized;
			cohe.GetComponent<Rigidbody>().AddForce(direcc * 10f, ForceMode.Impulse);
			cohe.transform.DOLookAt(point, 1f, AxisConstraint.None, cohe.transform.up);
			cohe.transform.DOScale(2f, 1f);
			audio.PlayOneShot(RocketSound,0.5f);
    }
    public void DispararCohetes() {
        StartCoroutine(_DispararCohetes());
    }
    IEnumerator _DispararCohetes() {
        Fruit[] AllFruits = FindObjectsOfType<Fruit>();

        for (int i = 0; i < AllFruits.Length; i++){
            GameObject cohe = Instantiate(Rocket, GameObject.Find("DisparadorCohete").transform.position, Rocket.transform.rotation) as GameObject;
            Destroy(cohe, 2);
            //cohe.transform.DOMove(point, 1f, false);        
            //Vector3 direcc = (AllFruits[i].transform.position - cohe.transform.position).normalized;
            //cohe.GetComponent<Rigidbody>().AddForce(direcc * 12f, ForceMode.Impulse);
            //cohe.transform.DOLookAt(AllFruits[i].transform.position, 0.5f, AxisConstraint.None, cohe.transform.up);
            cohe.transform.DOScale(1f, 1f);
            yield return new WaitForSeconds(0.1f);                        
        }        
    }
	
  
}
