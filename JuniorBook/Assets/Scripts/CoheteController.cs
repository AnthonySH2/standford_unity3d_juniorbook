﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CoheteController : MonoBehaviour {

	[SerializeField]
	float collisionEffectTime;
	[SerializeField]
	float coheteSpeed;
	[SerializeField]
	float MinDistance;

	GameObject target;
	Vector3 shadow;
	Vector3 PosInitial;
	private bool find;
	private float timeAnimate;
	private float startTime;
	private float journeyLength;
	int indexTarget = -1;
	float score;

	// Use this for initialization
	void Start () {

		score = float.Parse(GameObject.Find ("TextMeshProTextScorePicked").GetComponent<TextMeshProUGUI> ().text);
		GetComponent<Rigidbody>().AddForce(transform.forward * coheteSpeed, ForceMode.Impulse);
		//Invoke("encontrarTarget", 0.4f);
		startTime = Time.time;
		//FindByFruits();
		encontrarTarget();
	}
	// Update is called once per frame
	void Update () {
		// if (!target && find)
		if (target){
			float distCovered = (Time.time - startTime) * coheteSpeed;
			float fracJourney = distCovered / journeyLength;
			transform.LookAt(target.transform.position);            
			GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(PosInitial, target.transform.position, fracJourney));
			shadow=target.transform.position+transform.forward*2f;
		}else {
            float distCovered = (Time.time - startTime) * coheteSpeed;
			float fracJourney = distCovered / journeyLength;
			GetComponent<Rigidbody>().MovePosition(Vector3.Lerp(PosInitial, shadow, fracJourney));
			//FindByFruits();
			//GetComponent<Rigidbody>().AddForce(transform.forward * 5f, ForceMode.Force);
			//GetComponent<Rigidbody>().MoveRotation(Quaternion.Euler(this.transform.eulerAngles + transform.right + transform.up));                
         }
	}

    private void OnCollisionEnter(Collision collision){
        if (collision.gameObject.tag == "Fruit")
        {
            string fruitName;
            float damage;
            float score0;
            //Coger fruta
            collision.gameObject.GetComponent<Fruit>().PickFruit(collisionEffectTime, out fruitName, out damage, out score0);
            //incrementar el score
            DoScore(score0);
            DoDamage(damage);
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag == "piso") {
            Destroy(this.gameObject);
        }
    }
    public void DoDamage(float percent){

      /*  float healthAux =   gameObject.GetComponent<FruitsController>().health;
        healthAux = Mathf.Clamp(healthAux + percent, 0f, 1f);
        Image imageHealthBar = gameObject.GetComponent<FruitsController>().ImageHealthBar;
        imageHealthBar.fillAmount = healthAux;
        imageHealthBar.sprite = gameObject.GetComponent<FruitsController>().HealthBars[0];*/
    }
    public void DoScore(float score0){
		score += score0;
		//incrementar el score
		GameObject.Find ("TextMeshProTextScorePicked").GetComponent<TextMeshProUGUI>().text = ((int)score).ToString ();
	}   
    void CanFind() {
        find = true;
    }
    void FindByFruits() {     
        foreach (var item in FindObjectsOfType<Fruit>()){
            if (Vector3.Distance(this.transform.position, item.transform.position) < MinDistance)
            {
                MinDistance = Vector3.Distance(this.transform.position, item.transform.position);
                target = item.gameObject;
            }
        }if (target){
            timeAnimate = 0f;
            PosInitial = this.transform.position;
            startTime = Time.time;            
            journeyLength = Vector3.Distance(PosInitial, target.transform.position);            
        }        
      //  Debug.Log(target.name);
    }
    void encontrarTarget() {
        foreach (var item in FindObjectsOfType<Fruit>())
        {
            if (!item.Marked)
            {
               target = item.gameObject;
                item.Marked = true;
                break;
            }
            
        }
        if (target)
        {
            timeAnimate = 0f;
            PosInitial = this.transform.position;
            startTime = Time.time;
            journeyLength = Vector3.Distance(PosInitial, target.transform.position);

        }
    }
    
   
}
