﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants{
    //Background
    public const int AMOUNTBACKGROUND = 2;

	//Fruits
	public const string LOWENERGY = "lowEnergy";
	public const string BOMB = "bomba";
	public const string HEARTH = "corazon";
	public const string ENERGY = "energy";
	public const int MAXFRUITSREPEAT = 3;
	public const int MAXENEMIESFRUITREPEAT = 1;

    //Numbers
    public const int MINVALUENUMBERS = 0;
    public const int MAXVALUENUMBERS = 20;
    public const float SECONDSTOREST = 0.05f;
    public const float SECONDSTOADD = 0.5f;
    


	//Character
	public const string ALEGRE = "Alegre";
	public const string ENOJADO = "Enojado";
	public const string ENAMORADO  = "Enamorado";
	public const string IDLE = "Idle";

	//levels
	public const float MINSPEEDLEVEL = 0.9f;
	public const float AMOUTTOFILLBUTTONSPECIAL = 0.025f;
	public const int SCORETOSTARTFASTOPTION = 4000;
}
