﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class Fruit : MonoBehaviour {

	[SerializeField]
	GameObject EfectoCollision;
	[SerializeField]
	float collisionEffectTime;
	[SerializeField]
	float gravityScale;
	[SerializeField]
	bool marked = false;
	[SerializeField]
	float damage;
	[SerializeField]
	float fallSpeed;
	[SerializeField]
	float score;
	[SerializeField]
	GameObject scoreFruit;
	[SerializeField]
	GameObject scoreImage;
    bool endScoreAnimation = false;

	bool doCollision = false;
	string collisionFruitName;

	Vector3 mygravity = new Vector3 (0f, -9.8f);

    float Duracion;

	void Start () {

		scoreFruit = GameObject.Find ("score").gameObject;
		scoreImage = GameObject.Find ("ScoreImage").gameObject;
		GetComponent<Rigidbody> ().useGravity = false;
		GetComponent<Rigidbody> ().AddRelativeTorque (Random.onUnitSphere);      	
	}

    public void SetDuracion(float dur) {
        Duracion = dur;
    }

	// Update is called once per frame
	void FixedUpdate () {
		GetComponent<Rigidbody>().AddForce(0,-0.05f, 0);
	}

	void OnCollisionEnter(Collision col){

		if (col.gameObject.tag == "Piso") {

			GameObject goCollisionEffect = Instantiate (EfectoCollision,this.transform.position,EfectoCollision.transform.rotation) as GameObject;			
			Destroy (this.gameObject);
			Destroy (goCollisionEffect, collisionEffectTime);
			doCollision = true;
			collisionFruitName = this.name;			
		}
	}

	public void PickFruit(float collisionEffectTime, out string fruit, out float damage, out float score)
    {
		GameObject goCollisionEffect = Instantiate (EfectoCollision,this.transform.position,EfectoCollision.transform.rotation) as GameObject;

		fruit = this.gameObject.name;
		damage = this.damage;
        score = this.score;
		Destroy (this.gameObject);
		Destroy (goCollisionEffect, collisionEffectTime);

		Debug.Log (scoreFruit==null);
		GameObject scoreByFruit = Instantiate (scoreFruit, this.transform.position, scoreFruit.transform.rotation) as GameObject;
		scoreByFruit.GetComponentInChildren<Text> ().text = score.ToString();

        StartCoroutine(movetoScore(scoreByFruit, goCollisionEffect));        		
	}

    IEnumerator movetoScore(GameObject scoreByFruit, GameObject goCollisionEffect) {

        scoreByFruit.transform.DOScale(new Vector3(0.02f, 0.02f, 1f), 1f).SetEase(Ease.InOutSine).SetDelay(0.2f).OnPlay(() => {
            //  scoreFruit.transform.DOShakePosition(0.5f, 1f, 5,0f,false,false);
            scoreByFruit.transform.DOMove(scoreImage.transform.position, 1f).SetEase(Ease.InOutCirc).OnComplete(() => {
                Destroy(scoreByFruit);
                Destroy(goCollisionEffect, collisionEffectTime);
                endScoreAnimation = true;
            });
        });

        yield return new WaitUntil(() => endScoreAnimation == true);        
    }

	public bool Marked{
		get{ return marked; }
		set{ marked = value; }
	}
	public bool DoCollision{
		get{ return doCollision; }
		set{ doCollision = value;}
	}
	public string CollisionFruitName{
		get{ return collisionFruitName; }
	}
	public float Damage{
		get{ return damage;}
	}
}
