﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class FruitsController : MonoBehaviour {

    public GameObject levelPanel;
    public GameObject LevelGameOverPanel;
    public LayerMask mask;
    public List<PropertiesLevel> characterDialogsByLevel;
    public List<PrefabController> prefabsfruitsToThrow;
    public List<PrefabController> prefabsEnemiesfruits;
    public List<Sprite> HealthBars;
    public List<List<String>> dialogsText;
    public GameObject scoreByFruit;
    public bool canplay = false;
    public int maxLevel;
    public float health;
    public float specialButtonValue;
    public float criticalHealth;
    public float collisionEffectTime;
    public static float score;
    public int fruitsPickedInARow = 0;
    public int fruitsFallInARow = 0;
    public float fallSpeed;
    public int minApearingInterval;
    public int maxApearingInterval;

    GameObject target;
    static GameObject goCharacter;
    CharacController character;
    int characterSelected;

    Button buttonHomeFinal;
    Button buttonHomeBG;
    Button buttonRePlay;
    Button buttonSpecialRocket;
    Button buttonHomeButtonScene;
    Button buttonHomeButtonEndGame;
    Image imageSpecialRocket;
    Image imageHealthBar;
    TextMeshProUGUI scoreGUI;
    TextMeshProUGUI levelNumber;
    List<GameObject> fruits;
    List<PrefabController> fruitsForLevel;
    List<int> listCantFruitsForLevel;
    int index;
    int level;
    int fruitNow;
    int fruitBefore;
    int counterInterval;
    int countEnergyHearth;
    int intervalApearing;
    float finalAngle = 180;
    float speedRotation = 5;
    static bool ready = false;

    void Start() {

        if (!ready) {
            InitComponents();
            ShowBotMessage();        }
        InitLevel();
    }

    void InitComponents() {

        //TopPanel
        //Asignar imagenes barra de vida
        //BottonPanel
        //Asign images to button Special, ChallengeBar and Score

        GameObject gohealtBackBar = GameObject.Find("TopPanel/HealthBackBar");
        GameObject goSpecialButton = GameObject.Find("BottomPanel/SpecialButton");
        GameObject goChallengeBar = GameObject.Find("BottomPanel/Challenger/Bar");
        GameObject goScoreImage = GameObject.Find("BottomPanel/ScoreImage");
        
        goCharacter = Instantiate(Resources.Load<GameObject>(string.Format("_character/{0}", characterSelected)));
        character = goCharacter.gameObject.GetComponent<CharacController>();
        listCantFruitsForLevel = new List<int>();
        fruits = new List<GameObject>();
        fruitsForLevel = new List<PrefabController>();

        levelPanel = GameObject.Find("PanelNextLevel").gameObject;
        LevelGameOverPanel = GameObject.Find("PanelEndOverGame").gameObject;
        buttonHomeFinal = GameObject.Find("ButtonHomeFinal").GetComponent<Button>();
        target = GameObject.Find("Targets").gameObject;
        buttonHomeBG = GameObject.Find("Home").GetComponent<Button>();
        buttonRePlay = GameObject.Find("ButtonPlay").GetComponent<Button>();
        buttonHomeButtonScene = GameObject.Find("TopPanel/Buttons/Home").GetComponent<Button>();
        buttonHomeButtonEndGame = GameObject.Find("PanelEndOverGame/ButtonHomeFinal").GetComponent<Button>();

        UnityEngine.UI.Image[] specialImages = goSpecialButton.GetComponentsInChildren<UnityEngine.UI.Image>();
        buttonSpecialRocket = goSpecialButton.GetComponentInChildren<Button>();

        int j = 0;

        do {
            if (specialImages[j].name == "SpecialButton") {
                specialImages[j].sprite = Resources.Load<Sprite>(string.Format("game1/_images/special/{0}", 0));
            } else {
                specialImages[j].sprite = Resources.Load<Sprite>(string.Format("game1/_images/special/{0}", 2));
                imageSpecialRocket = specialImages[j];
            }
            j++;

        } while (j < specialImages.Length);

        goChallengeBar.GetComponentInChildren<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>(string.Format("game1/_images/bar{0}", 1));
        goScoreImage.GetComponentInChildren<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>(string.Format("game1/_images/score{0}", 0));

        buttonHomeBG.onClick.AddListener(BackHome);
        buttonHomeFinal.onClick.AddListener(BackHome);
        buttonRePlay.onClick.AddListener(Replay);
        buttonSpecialRocket.onClick.AddListener(ThrowRockets);
        buttonHomeButtonEndGame.onClick.AddListener(GoHome);
        buttonHomeButtonScene.onClick.AddListener(GoHome);

        //Health bar components
        UnityEngine.UI.Image[] itemsHealth = gohealtBackBar.GetComponentsInChildren<UnityEngine.UI.Image>();
        j = 1;

        gohealtBackBar.GetComponent<UnityEngine.UI.Image>().sprite = Resources.Load<Sprite>(string.Format("game1/_images/health/{0}", j - 1));
        do {
            if (itemsHealth[j].name == "HealthBar" + j) {
                itemsHealth[j].sprite = Resources.Load<Sprite>(string.Format("game1/_images/health/{0}", j));
                if (j == 1) {
                    imageHealthBar = itemsHealth[j];
                }
            }
            j++;
        } while (j < itemsHealth.Length);

        //Score components
        TextMeshProUGUI[] textMeshScoreProperties = goScoreImage.GetComponentsInChildren<TextMeshProUGUI>();
        j = 0;

        do {
            if (textMeshScoreProperties[j].name == "TextMeshProTextScorePicked") {
                scoreGUI = textMeshScoreProperties[j];
                break;
            }
            j++;
        } while (j < textMeshScoreProperties.Length);

        specialButtonValue = 0;
        Debug.Log("Activando paneles");
    }
    void InitLevel() {

        levelPanel.gameObject.SetActive(true);
        if (target != null) {
            target.SetActive(false);
        }        
        TextMeshProUGUI TextMeshScore = GameObject.Find("TextMeshProTextScore").gameObject.GetComponentInChildren<TextMeshProUGUI>();
        TextMeshScore.text = "0";
        LevelGameOverPanel.gameObject.SetActive(false);
        ready = true;
        levelPanel.transform.localScale = Vector3.zero;
        levelPanel.transform.DOScale(1, 1).SetEase(Ease.OutElastic).SetDelay(0.5f).OnComplete(() => {

            Debug.Log("Complete");

            //Se busca panel de nivel para modificarlo
            if (levelPanel.gameObject.GetComponentsInChildren<TextMeshProUGUI>()[0].name == "TextMeshProTextLevel") {
                levelNumber = levelPanel.gameObject.GetComponentsInChildren<TextMeshProUGUI>()[0];
            }
            if (levelPanel.gameObject.GetComponentsInChildren<TextMeshProUGUI>()[1].name == "TextMeshProTextLevel") {
                levelNumber = levelPanel.gameObject.GetComponentsInChildren<TextMeshProUGUI>()[1];
            }

            levelPanel.gameObject.SetActive(false);
            level = 1;
            score = 0;
            health = 1f;
            canplay = true;
            fruitNow = 0;
            fruitBefore = 0;
            counterInterval = 0;
            countEnergyHearth = 0;
            specialButtonValue = 0;
            imageHealthBar.fillAmount = 1;
            imageHealthBar.sprite = HealthBars[0];
            imageSpecialRocket.fillAmount = 0f;
            intervalApearing = UnityEngine.Random.Range(minApearingInterval, maxApearingInterval);
            setfruitforlevel(level);
            Debug.Log("intervalApearing " + intervalApearing);
            InvokeRepeating("throwFruit", 0, Constants.MINSPEEDLEVEL);

        });

    }

    void FixedUpdate() {

#if !Unity_android

        //If user can play
        if (canplay) {

            //When right click
            if (Input.GetMouseButtonDown(0))
            {
                //Ray point to mouse 
                Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit pisoHit, hit;

                if (Physics.Raycast(rayo, out pisoHit, 105f, mask) && Physics.Raycast(rayo, out hit))
                {
                    //If fruits to pick up left and healt is good
                    if (fruitsForLevel.Count > 0 && health > 0) {
                        PickFruits(hit);
                        if (health <= 0) {
                            //End game to bad health
                            canplay = false;
                            GameOver();
                            return;
                        }
                    } else if (fruitsForLevel.Count == 0 && health > 0) {
                        //There is no fruits and healt is good
                        canplay = false;
                        CancelInvoke("throwFruit");
                        level++;
                        //Go to next level
                        if (level <= maxLevel) {
                            ChangeLevel();
                        } else {
                            //When reach to last level
                            WinGame();
                        }
                    } else if (health <= 0) {
                        //End game to bad health
                        canplay = false;                       
                        GameOver();
                        return;
                    }
                    Debug.Log(score);
                }
            }

        }
#else

		if (Input.touchCount == 1)
		{
		Ray rayo = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
		RaycastHit pisoHit, hit;
		if (Physics.Raycast(rayo, out pisoHit, 105f, mask) && Physics.Raycast(rayo, out hit))
		{
		if (Input.GetTouch(0).phase == TouchPhase.Began)
		{
		Debug.Log("Touch one");
		//hit.transform.gameObject.GetComponent<Fruit> ().PickFruit ();
		FindObjectOfType<RobotController>().DispararCohete(hit.point);
		}
		if (Input.GetTouch(0).phase == TouchPhase.Moved){
		}
		if (Input.GetTouch(0).phase == TouchPhase.Ended
		|| Input.GetTouch(0).phase == TouchPhase.Canceled){
		}
		}
		}
#endif
    }
    void GoHome() {
        FindObjectOfType<SceneController>().LoadARScene();       
    }
    void ShowBotMessage(){        

    }
    void ThrowRockets(){
        character.DispararCohetes();
        buttonSpecialRocket.interactable = false;
        imageSpecialRocket.fillAmount = 0;
        specialButtonValue = 0;
    }
    void PickFruits(RaycastHit hit){
		string fruitName;
		float damage;
		float score0;
		//Pick up fruit
		hit.transform.gameObject.GetComponent<Fruit> ().PickFruit (collisionEffectTime,out fruitName, out damage, out score0);
		DoScore(score0);
        DoDamage(damage);
        SetCharacterState(fruitName);

        if (fruitName == Constants.ENERGY){
          DoFillSpecialButton(10);
        }else {
          DoFillSpecialButton(1);
        }
	}
	void ChangeLevel(){
		//change character state
		character.setState (Constants.ALEGRE);
		character.gameObject.transform.DORotate(new Vector3 (0,360,0), 1f, RotateMode.LocalAxisAdd).SetDelay(2f).OnComplete(() =>{ 
			levelPanel.gameObject.SetActive (true);
			levelPanel.transform.localScale= Vector3.zero;
			levelNumber.text = level.ToString();
			//panel level animation
			levelPanel.transform.DOScale (1, 1).SetEase (Ease.OutElastic).SetDelay (0.5f).OnComplete (() => {		
				levelPanel.transform.DOScale(0,0.5f).SetEase(Ease.InBack).SetDelay(0.5f).OnComplete(()=>{
					levelPanel.gameObject.SetActive (false);
					fruitNow = 0;
					fruitBefore = 0;
					counterInterval = 0;
					countEnergyHearth = 0;
					//amount of fruits that have to appear before special items(hearth and energy)
					intervalApearing = UnityEngine.Random.Range(minApearingInterval, maxApearingInterval);
					setfruitforlevel (level);
					canplay = true;
					//Initialize fruit thrower again with higher frecuency
					InvokeRepeating ("throwFruit", 0, Constants.MINSPEEDLEVEL-((level + 1)/10f));
				});
			});
		});
	}
	void WinGame(){		
		character.setState (Constants.ALEGRE);
		character.gameObject.transform.DORotate(new Vector3 (0,360,0), 1f, RotateMode.WorldAxisAdd).SetLoops(5, LoopType.Incremental).SetDelay(1f).OnComplete(() =>{
            
            ShowFinalPanel();
        });
	}
	void GameOver(){
		canplay = false;
		CancelInvoke("throwFruit");
		character.setState (Constants.ENOJADO);
        //character.turnCharacter(0,finalAngle,0,speedRotation);
        character.transform.DORotate(new Vector3(0, finalAngle,0), speedRotation, RotateMode.LocalAxisAdd).SetDelay(2.5f).SetEase(Ease.InOutSine).OnComplete(() => {
            canplay = false;
            cancelInvoke();
            ShowFinalPanel();
        });
        
	}
	void ShowFinalPanel(){

        LevelGameOverPanel.SetActive(true);
        LevelGameOverPanel.transform.DOScaleZ(0,1f).SetEase(Ease.OutSine).OnComplete(()=>{
            LevelGameOverPanel.transform.DOScaleZ(1, 0.5f).SetEase(Ease.InSine);            
            StartCoroutine(FinalScore());
        });
       
        goCharacter.SetActive(false);
	}

    IEnumerator FinalScore()
    {
        int i = 0;
        TextMeshProUGUI TextMeshScore = GameObject.Find("TextMeshProTextScore").gameObject.GetComponentInChildren<TextMeshProUGUI>();
        do
        {
            TextMeshScore.text = i.ToString();
            yield return new WaitForSeconds(0.0002f);
                i = i + 1500;
        } while (i < score);
        //Si el score es menor a 1500
        if (int.Parse(TextMeshScore.text) < score) {
            TextMeshScore.text = score.ToString();
        }

    }
    void BackHome() {
    }

    void Replay() {
        levelPanel.SetActive(true);
        goCharacter.SetActive(true);
        character.transform.Rotate(0, 180, 0);
        Start();
    }

    void IncrementAmountFruit(string fruitName){
	
		int index = 0;
		int cantFruitPicked = 0;
        float amountFillImage = 0f;
        
        do
        {	
			if (fruitName == fruitsForLevel[index].Name) {

                GameObject goTarget = GameObject.Find("Target" + fruitsForLevel[index].ChallengeOrder);
                TextMeshProUGUI[] targetProperties = goTarget.GetComponentsInChildren<TextMeshProUGUI>();
               
                int indexj = 0;

                do {
                    if (targetProperties[indexj].name == "TextMeshTargetPicked"){
                        string FruitPicked = targetProperties[indexj].GetComponent<TextMeshProUGUI>().text;
                        Debug.Log("FRUIT PICKED 0" + FruitPicked);
                        cantFruitPicked = int.Parse(FruitPicked);
                        cantFruitPicked++;                       
                        amountFillImage = cantFruitPicked / (fruitsForLevel[index].AmounttoPick*1.0f);
                        targetProperties[indexj].GetComponent<TextMeshProUGUI>().text = cantFruitPicked.ToString();
                    }                    
                    indexj++;
                } while (indexj<targetProperties.Length);

                Image[] imgtarget = goTarget.GetComponentsInChildren<Image>();
                int indexn = 0;

                do {
                    if(imgtarget[indexn].name == "Image"+index) {
                        imgtarget[indexn].fillAmount = amountFillImage;
                        break;}
                    indexn++;
                } while (indexn<imgtarget.Length);
                
                //transform.Find(string.Format("Target{0}/TextMeshTargetPicked",fruitsForLevel[index].ChallengeOrder)).GetComponent<TextMeshProUGUI>().text;

                if (cantFruitPicked == fruitsForLevel[index].AmounttoPick){
					fruitsForLevel.Remove (fruitsForLevel[index]);					
					break;
				}

				Debug.Log("cantFruitPicked " + cantFruitPicked);
				Debug.Log("listCantFruitsForLevel " + fruitsForLevel[index].AmounttoPick);
			}

			index++;

		}while (index<fruitsForLevel.Count);
	}

    //Fill Fruits to appear on challenge bar
	void setfruitforlevel(int level){
		Debug.Log ("NEXT LEVEL " + level);
		fruitsForLevel = new List<PrefabController> ();
		PrefabController[] fruitsToThrowAux = new PrefabController[prefabsfruitsToThrow.Count];
		prefabsfruitsToThrow.CopyTo (fruitsToThrowAux);
		//Set initialfruits to pick on level
		int i=0;

		do{
			
		    int fruitIndex = getFruitIndex ();

		    if (prefabsfruitsToThrow [fruitIndex].CanPick){
			    int cantfruitsForLevel = (level) * 8 + fruitIndex;
			    prefabsfruitsToThrow [fruitIndex].ChallengeOrder = i;
                prefabsfruitsToThrow[fruitIndex].AmounttoPick = cantfruitsForLevel;
                //Frutas a mostrar por cada nivel
			    fruitsForLevel.Add (prefabsfruitsToThrow [fruitIndex]);              
			    Debug.Log("index " + fruitIndex);
			    prefabsfruitsToThrow.RemoveAt (fruitIndex);	 
			    i++;
		    }

		}while(i<level);

		Debug.Log ("FRUITS FOR LEVEL " + fruitsForLevel.Count);
		prefabsfruitsToThrow.Clear ();

		foreach (PrefabController obj in fruitsToThrowAux) {
			prefabsfruitsToThrow.Add (obj);
		}
		setFruitsForPlay ();
		showChallengeMessage ();
	}

	//get fruits index to pick
	int getFruitIndex(){		
		int fruitIndexAux = UnityEngine.Random.Range (0, prefabsfruitsToThrow.Count-1);
		return fruitIndexAux;
	}
	//Fill fruits to play for
	void setFruitsForPlay(){
	
		int fruitCount = 0;
		int fruitEnemiesCount = 0;
		int index = 0;
		int indexFruitToThrow = 0;
		int indexFruitEnemies = 0;
			
		Debug.Log ("Fruits to throw " + prefabsfruitsToThrow.Count);

		do {

			if (indexFruitEnemies< prefabsEnemiesfruits.Count)
			{
				fruits.Add (prefabsEnemiesfruits [indexFruitEnemies].Prefab);
				fruitEnemiesCount++;

				if (fruitEnemiesCount<=Constants.MAXENEMIESFRUITREPEAT){
					indexFruitEnemies++;	
				}
			}
			if (index < fruitsForLevel.Count) {
				fruits.Add (fruitsForLevel [index].Prefab);
				fruitCount++;

				if (fruitCount == level) {
					fruitCount = 0;
					index++;
				}

			} if(indexFruitToThrow < prefabsfruitsToThrow.Count) {
				//if (prefabsfruitsToThrow [indexFruitToThrow].CanPick) {
					fruits.Add (prefabsfruitsToThrow [indexFruitToThrow].Prefab);	
				//}
				Debug.Log("indexFruitToThrow "  + indexFruitToThrow);
				indexFruitToThrow++;
			}

		} while(indexFruitEnemies< prefabsEnemiesfruits.Count || index < fruitsForLevel.Count ||  indexFruitToThrow < prefabsfruitsToThrow.Count);

		Debug.Log ("Cant frutas " + fruits.Count);
	}
	void showChallengeMessage(){

		target.SetActive (true);
		for (int j = 0; j< target.GetComponentsInChildren<Image>(true).Length ; j++) {
			target.GetComponentsInChildren<Image>(true)[j].gameObject.SetActive (true);
		}

		Debug.Log ("Show Challenge FruitForlevel " + fruitsForLevel.Count);

		for (int i = 0; i < fruitsForLevel.Count; i++) {

            GameObject goTarget = GameObject.Find("Target" + i);
            Image[] imgTarget = goTarget.GetComponentsInChildren<Image>();
            int x = 0;
            do{
                if (imgTarget[x].name == "Target" + i){
                    imgTarget[x].sprite = fruitsForLevel[i].ImagePrefabGrey;
                }
                else{   
                    imgTarget[x].sprite = fruitsForLevel[i].ImagePrefab;
                    imgTarget[x].fillAmount = 0;
                }
                x++;
            } while (x < imgTarget.Length);
                                   
            TextMeshProUGUI[] targetProperties = goTarget.GetComponentsInChildren<TextMeshProUGUI>();
            int j = 0;
            do{
                if (targetProperties[j].name == "TextMeshTargetToPick"){
                    targetProperties[j].text = "/" + fruitsForLevel[i].AmounttoPick;
                }
                else if (targetProperties[j].name == "TextMeshTargetPicked"){
                    targetProperties[j].text = "0";
                }                
                j++;
            } while (j < targetProperties.Length);            
        }
		for (int j = fruitsForLevel.Count; j < 5; j++) {
			GameObject.Find ("Target" + j).GetComponentInChildren<Image> ().gameObject.SetActive (false);
		}
	}
	public void cancelInvoke(){
		CancelInvoke ("throwFruit");	
	}
	public void StopPlay(){
		canplay = false;
	}

	public void DoFillSpecialButton(int amountToAdd){	
		if (specialButtonValue < 1) {
			specialButtonValue = Mathf.Clamp (specialButtonValue + Constants.AMOUTTOFILLBUTTONSPECIAL*amountToAdd, 0f, 1f);
            imageSpecialRocket.fillAmount = specialButtonValue;
		} else {			
            buttonSpecialRocket.interactable = true;
        }
	}

	public void DoScore(float score0){
		score += score0;
		//incrementar el score
		scoreGUI.text = ((int)score).ToString ();
	}

	public void DoDamage(float percent){
		health = Mathf.Clamp (health + percent, 0f, 1f);
		imageHealthBar.fillAmount = health;
		imageHealthBar.sprite = HealthBars [0];		
	}

	void SetCharacterState(string fruitName){		
		switch (fruitName) {
		case Constants.HEARTH:
			character.setState (Constants.ENAMORADO);
			break;
		case Constants.ENERGY:
			character.setState (Constants.ALEGRE);							
			break;
		case Constants.BOMB:
			character.setState (Constants.ENOJADO);
			break;
		default:
			IncrementAmountFruit(fruitName);
			break;
		}
	}
	void throwFruit(){

		Debug.Log("THROWFRUIT canplay " + canplay);
		if (canplay) {
			
			Debug.Log ("Index FRUTA : " + index);
			index = UnityEngine.Random.Range (0, fruits.Count);
			fruitNow = index;

			if (((fruits [index].name == Constants.HEARTH) || (fruits [index].name ==Constants.ENERGY)) && countEnergyHearth>=2){
				Debug.Log("Fruit " + fruitNow);
				if (counterInterval<intervalApearing) {					
					do{
						index = UnityEngine.Random.Range (0, fruits.Count);
					}while((fruits [index].name == Constants.HEARTH) || (fruits [index].name == Constants.ENERGY));
				}if (counterInterval == intervalApearing){
					if (countEnergyHearth >= 2) {
						countEnergyHearth = 0;
					}			
				}			
			}

			if (fruits [index].name == "energy" || fruits [index].name == "corazon") {
				countEnergyHearth++;
				counterInterval = 0;			
			}

			GameObject fruit=Instantiate (fruits [index], this.transform.position + Vector3.right * UnityEngine.Random.Range (-6f, 6f)+ Vector3.up * UnityEngine.Random.Range (0, 6f), 
			Quaternion.Euler (UnityEngine.Random.Range (-180f, 180f) * UnityEngine.Random.onUnitSphere)) as GameObject;
			
			fruit.name = fruits[index].name;
            fruit.GetComponent<Fruit>().SetDuracion(2f);			

			fruitBefore = index;
			counterInterval+=1;
		}
	}

	public List<GameObject> Fruits{
		get{ fruits = new List<GameObject> ();
			return fruits; }
		set{fruits = value; }		
	}
    public GameObject GoCharacter{
        set { goCharacter = value; }
    }
    public Image ImageHealthBar {
        set { imageHealthBar = value; }
        get { return imageHealthBar; }
    }
    public int CharacterSelected {
        set { characterSelected = value; }
        get { return characterSelected; }
    }

}
