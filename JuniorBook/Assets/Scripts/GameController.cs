﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class GameController : MonoBehaviour {

    public UnityEngine.UI.Image imgBackground;
    public GameObject goTopPanel;
    public GameObject goBottonPanel;
    public List<Games> GameList;
    public static bool active = false;
    GameObject character;
    int characterSelected;
    int gameNumber;
    
   
	void Start () {
       
        gameNumber  = 2;// Random.Range (1, gameNumbers);
        characterSelected=FindObjectOfType<SceneController>().targetCharacter;
        InitComponents();
        StartPlay();

        //Challenger back Color
        /*ChallengerBackColor = new string[6];
        ChallengerBackColor[0] = "#9CC918FF";
        ChallengerBackColor[1] = "#FC2E58FF";
        ChallengerBackColor[2] = "#B42A83FF";
        ChallengerBackColor[3] = "#00000000";
        ChallengerBackColor[4] = "#592A83FF";
        ChallengerBackColor[5] = "#E47D17FF"; */

    }
    void InitComponents(){
        //Elegir un fondo ramdom para cambiar background
        int backgroundNumber = Random.Range(1, Constants.AMOUNTBACKGROUND);
        Debug.Log("backgroundNumber" + backgroundNumber);
        active = true;
        imgBackground.sprite = Resources.Load<Sprite>(string.Format("_background/{0}", backgroundNumber));
    }
    public void StartPlay(){
		switch (gameNumber) {
			case 1: FrutsGame(gameNumber);break;
			case 2:	BubbleNumberGame(gameNumber);break;		
		}
	}
	public void BubbleNumberGame(int gameNumber){
        if (GameList[gameNumber - 1].objectController != null)
        {
            GameList[gameNumber - 1].objectController.GetComponent<NumberController>().CharacterSelected = characterSelected;
        }
        InstantiateNumberController(gameNumber);
    }
	public void FrutsGame(int gameNumber){
        if (GameList[gameNumber - 1].objectController != null)
        {
            GameList[gameNumber - 1].objectController.GetComponent<FruitsController>().CharacterSelected = characterSelected;
        }
        InstantiateFruitController (gameNumber);
	}

	public void PauseGame(){
        //InstruccionsPanel.SetActive (false);      
	}

	public void enfocar(){
		bool focusModeSet = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
		if (!focusModeSet){
			Debug.Log("Failed to set focus mode (unsupported mode).");
		}
	}

	/*public void SetRobotActivo(RobotImageTrackable rob){
		//robotActivo=rob;
	}

	public RobotImageTrackable GetRobotActivo(){
		//return robotActivo;
	}*/

	public void InstantiateFruitController(int gameNumber){
		if (GameList.Count > 0 ) {
			if (GameList [gameNumber-1].objectController != null) {
              //  GameList[gameNumber - 1].fruitsController.GetComponent<FruitsController>().GoCharacter = character;
                Instantiate (GameList [gameNumber-1].objectController, GameObject.Find ("FruitThrower").transform.position, 
				GameList [gameNumber-1].objectController.transform.rotation, GameObject.Find ("FruitThrower").transform);
			} else {
				Debug.Log ("<color=red>Fatal Error, Niveles is not setup</color>");
			}
		} else {
			Debug.Log ("<color=red>Fatal Error, Niveles is not setup</color>");
		}				
	}
    public void InstantiateNumberController(int gameNumber)
    {
        if (GameList.Count > 0)
        {
            if (GameList[gameNumber - 1].objectController != null) {
                //  GameList[gameNumber - 1].fruitsController.GetComponent<FruitsController>().GoCharacter = character;
                Instantiate(GameList[gameNumber - 1].objectController, GameObject.Find("NumberThrower").transform.position,
                GameList[gameNumber - 1].objectController.transform.rotation, GameObject.Find("NumberThrower").transform);
            }
            else
            {
                Debug.Log("<color=red>Fatal Error, Niveles is not setup</color>");
            }
        }
        else{
            Debug.Log("<color=red>Fatal Error, Niveles is not setup</color>");
        }
    }
    public int CharacterSelected{
		get{return characterSelected; }
		set{characterSelected = value; }
	}
    public GameObject Character {
        get { return character; }
        set { character = value; }
    }
	[System.Serializable]
	public class Games{
		public string name;
		public float gravityScale;
		public int scoreToWin;
		public GameObject objectController;
	}

}
