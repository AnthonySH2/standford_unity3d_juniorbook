﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaskButtonAnimator : MonoBehaviour {
    public Vector3 Centralposition;
    public float Range = 100f;
    // Use this for initialization
    Vector3 myScale;
	void Start () {
        myScale = transform.localScale;
    }
	
	// Update is called once per frame
	void Update () {
       // Debug.Log(this.transform.position);
        float dist = Vector3.Distance(this.transform.position, Centralposition);
        if (dist > 0 && dist< Range) {
            float scaleMagnitude = (Range-dist)/(Range/2f);            
            Vector3 myScale2 = myScale *Mathf.Clamp(scaleMagnitude,1f,5f);            
            transform.localScale = myScale2;
        }
        
	}
}
