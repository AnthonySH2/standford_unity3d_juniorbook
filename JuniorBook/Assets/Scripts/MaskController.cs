﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskController : MonoBehaviour {
    public List<Texture> textures;
    public Material targetMaterial;
    // Use this for initialization
    int index;
    Texture currenttext;
	void Start () {
        index = 0;
        applyMask();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetMask(int id)
    {
        index=id;
        applyMask();
    }
    public void NextMask() {
        index++;
        if (index > textures.Count) {
            index = 0;
        }
        applyMask();
    }
    public void PreviousMask()
    {
        index--;
        if (index == 0)
        {
            index = textures.Count;
        }
        applyMask();
    }

    public void applyMask() {
        currenttext = textures[index];
        targetMaterial.mainTexture = currenttext;
    }
}
