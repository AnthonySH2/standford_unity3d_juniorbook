﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Number : MonoBehaviour {

    [SerializeField]
    GameObject EfectoCollision;
    [SerializeField]
    float collisionEffectTime;
    [SerializeField]
    float gravityScale;   
    [SerializeField]
    float damage;
    [SerializeField]
    float fallSpeed;
    [SerializeField]
    float score;
    float Duracion;
    int value;

    bool doCollision = false;
    string collisionFruitName;
    bool endScoreAnimation = false;
    
    Vector3 mygravity = new Vector3(0f, -9.8f);
    
    void Start(){
        value = Random.Range(Constants.MINVALUENUMBERS, Constants.MAXVALUENUMBERS);
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().AddRelativeTorque(Random.onUnitSphere);        
    }
    public void SetDuracion(float dur){
        Duracion = dur;
    }
    // Update is called once per frame
    void FixedUpdate(){
        GetComponent<Rigidbody>().AddForce(0, -0.05f, 0);
    }
    void OnCollisionEnter(Collision col){
        if (col.gameObject.tag == "Piso"){
            GameObject goCollisionEffect = Instantiate(EfectoCollision, this.transform.position, EfectoCollision.transform.rotation) as GameObject;
            Destroy(this.gameObject);
            Destroy(goCollisionEffect, collisionEffectTime);
            doCollision = true;
            collisionFruitName = this.name;
        }
    }
    
    public void PickNumber(float collisionEffectTime, out int value, out float damage, out float score) {
        GameObject goCollisionEffect = Instantiate(EfectoCollision, this.transform.position, EfectoCollision.transform.rotation) as GameObject;

        value = this.value;
        damage = this.damage;
        score = this.score;
        Destroy(this.gameObject);
        Destroy(goCollisionEffect, collisionEffectTime);
        
        //scoreByFruit.GetComponentInChildren<Text>().text = score.ToString();
        //StartCoroutine(movetoScore(scoreByFruit, goCollisionEffect));
    }    

    /*IEnumerator movetoScore(GameObject scoreByFruit, GameObject goCollisionEffect){

         //scoreByFruit.transform.DOScale(new Vector3(0.02f, 0.02f, 1f), 1f).SetEase(Ease.InOutSine).SetDelay(0.2f).OnPlay(() => {
             //scoreFruit.transform.DOShakePosition(0.5f, 1f, 5,0f,false,false);
             //scoreByFruit.transform.DOMove(scoreImage.transform.position, 1f).SetEase(Ease.InOutCirc).OnComplete(() => {
                 Destroy(scoreByFruit);
                 Destroy(goCollisionEffect, collisionEffectTime);
                 endScoreAnimation = true;
             });
         });

         yield return new WaitUntil(() => endScoreAnimation == true);
     }*/

    public bool DoCollision
    {
        get { return doCollision; }
        set { doCollision = value; }
    }
    public string CollisionFruitName
    {
        get { return collisionFruitName; }
    }
    public float Damage
    {
        get { return damage; }
    }
    public int Value
    {
        get { return value; }
    }
}
