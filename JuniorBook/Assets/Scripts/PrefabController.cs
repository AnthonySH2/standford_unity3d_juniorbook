﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PrefabController {

	[SerializeField]
	string name;
    [SerializeField]
    int amountToPick;
    [SerializeField]
	GameObject prefab;
	[SerializeField]
	Sprite imagePrefab;
    [SerializeField]
    Sprite imagePrefabGrey;   
    [SerializeField]
	bool canPick;
	[SerializeField]
	int challengeOrder = -1;

	public string Name{
		get{return name; }
		set{ name = value; }
	}
    public int AmounttoPick {
        get { return amountToPick; }
        set { amountToPick = value; }
    }
	public GameObject Prefab{
		get{ return prefab; }
		set{ prefab = value; }
	}
	public Sprite ImagePrefab{
		get{ return imagePrefab;}
		set{ imagePrefab = value;}
	}
    public Sprite ImagePrefabGrey {
        get { return imagePrefabGrey; }
        set { imagePrefabGrey = value; }
    }
    public bool CanPick{
		get{return canPick; }
		set{ canPick = value; }
	}
	public int ChallengeOrder{
		get{return challengeOrder; }
		set{ challengeOrder = value; }
	}

}
