﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="dialogs", menuName="fruitDialogsByLevel/Create DialogByLevel")]

public class PropertiesByLevel: ScriptableObject {

	[SerializeField]
	int level;
	[SerializeField]
	string characterMessage;
	[SerializeField]
	Sprite dialogBox;

	public int Level{
		get{ return level; }
		set{ level = value;}
	}
	public string CharacterMessage{
		get{ return characterMessage; }
		set{ characterMessage = value; }
	}
	public Sprite DialogBox{
		get{ return dialogBox;}
		set{ dialogBox = value;}
	}
	
}
