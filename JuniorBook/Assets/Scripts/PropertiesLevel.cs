﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PropertiesLevel {

	[SerializeField]
	int level;
	[SerializeField]
	List<string> characterMessage;
	[SerializeField]
	Sprite dialogBox;
	[SerializeField]
	int character;

	public int Level{
		get{ return level; }
		set{ level = value;}
	}
	public List<string> CharacterMessage{
		get{ return characterMessage; }
	}
	public Sprite DialogBox{
		get{ return dialogBox;}
		set{ dialogBox = value;}
	}
	public int Character{
		get{ return character; }
		set{ character = value;}
	}
}
