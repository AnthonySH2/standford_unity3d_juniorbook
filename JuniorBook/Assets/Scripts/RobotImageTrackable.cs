/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using Vuforia;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// A custom handler that implements the ITrackableEventHandler interface.
/// </summary>
public class RobotImageTrackable : MonoBehaviour,
                                                ITrackableEventHandler
    {
        #region PRIVATE_MEMBER_VARIABLES
 
        private TrackableBehaviour mTrackableBehaviour;

    #endregion // PRIVATE_MEMBER_VARIABLES

    public CharacController character;
    public List<GameObject> Effects;
	public List<Texture> screens;    
    
    public enum state
	{
		feliz,
		normal,
		triste,
		furioso
	}
	public state estado;
	bool startEffect=false;
    GameController game;
    

    //0: SpawnRobot 1: ThunderHead Robot

    #region UNTIY_MONOBEHAVIOUR_METHODS

   void Start()
   {

		game = FindObjectOfType<GameController> ();
		estado = state.normal;
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        
    }
	
        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

	
	public void SetScreen(string name){
		Texture choose=screens[0];

		foreach (var item in screens) {
			if (item.name == name) {
				choose = item; 
				break;
			}
		}
		GameObject.Find ("Pantalla").GetComponent<MeshRenderer> ().material.mainTexture = choose;


	}
        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }

		GameController.active = true;
        FindObjectOfType<ARController>().ActiveInstruccionPanel(false);
        

        if (!startEffect) {
			if (Effects.Count > 0) {
				GameObject go = Instantiate (Effects [0], transform.Find ("Piso").transform.position, Effects [0].transform.rotation, this.transform) as GameObject;
				Destroy (go, 5f);
			}
			startEffect = true;
		}
        if (character != null) { 
            FindObjectOfType<ARController>().setCharacter(character);
            if (character.name == "roboth_stanford")
            {
                FindObjectOfType<SceneController>().targetCharacter = 0;
            }
            character.StartAnim();
            FindObjectOfType<ARController>().GetCharacter().StartedDialog();
        }
    }


        private void OnTrackingLost()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

		GameController.active = false;
        FindObjectOfType<ARController>().ActiveInstruccionPanel(true);
        startEffect = false;
        if (character)
            character.DesactivarDialogos();
        }

  

    #endregion // PRIVATE_METHODS
}
