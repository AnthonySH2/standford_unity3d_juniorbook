﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using ULSTrackerForUnity;

public class SceneController : MonoBehaviour {
    public int targetCharacter;
    // Use this for initialization
    public SceneController instance;
	void Start () {
        if (instance == null) {
            instance = this;
        }
        if (instance != this)
            Destroy(this);
        DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadARScene(){
		if(SceneManager.GetActiveScene().name=="FaceJuniorBook")
			Plugins.ULS_UnityTrackerTerminate ();
		SceneManager.LoadScene ("_MainJunior");
	}

	public void LoadMaskScene(){
		if(SceneManager.GetActiveScene().name=="FaceJuniorBook")
			Plugins.ULS_UnityTrackerTerminate ();
		SceneManager.LoadScene ("FaceJuniorBook");
	}

	public void LoadMainGameScene(){      

        SceneManager.LoadSceneAsync ("MainGame");
	}
}
