#define DRAW_MARKERS

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ULSTrackerForUnity;
using UnityEngine.SceneManagement;

class FaceMask : MonoBehaviour
{	
#if DRAW_MARKERS
	public GameObject marker = null;
	List<GameObject> _marks = new List<GameObject>();
	bool drawMarkers = false;
#endif

	public GameObject faceMask = null;

	float[] _trackPoints = null;

	public Texture2D[] _masks = null;
	int maskTextureIndex = 0;

	private Mesh _mesh = null;
	private Vector3[] _vertices;

	bool initDone = false;

	void createMesh() {
		_trackPoints = new float[Plugins.MAX_TRACKER_POINTS*2];
		MeshFilter filter = faceMask.GetComponent<MeshFilter>();
		_mesh = filter.mesh;
		if (_mesh == null) {
			_mesh = new Mesh();
			filter.mesh = _mesh;
		}
		_vertices = _mesh.vertices;
	}

	void Start ()
	{
		InitializeTrackerAndCheckKey();
		createMesh ();
		Application.targetFrameRate = 60;

#if DRAW_MARKERS
		for (int i=0; i< Plugins.MAX_TRACKER_POINTS; ++i) {
			var g = Instantiate (marker);
            if(i!=30)
                g.name = "mark0" + i.ToString();
            else
                g.name = "MARK0" + i.ToString();
            g.transform.parent = transform.parent;
            g.GetComponent<MarkVisualizer>().setName(i.ToString());
			//g.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text=i.ToString();
			//g.SetActive (false);
			_marks.Add (g);
		}
        //print(_vertices.Length);
       /*
        for (int j = 0; j < Plugins.MAX_TRACKER_POINTS; ++j)
        {
            int v = j * 2;
            Vector3 pt = new Vector3(_vertices[j].x, _vertices[j].y, 0);

            _marks[j].transform.localPosition= (pt);
            //_marks[j].SetActive(drawMarkers);
        }       */
#endif
    }

	// Initialize tracker and check activation key
	void InitializeTrackerAndCheckKey ()
	{
		Plugins.OnPreviewStart = initCameraTexture;
		//Plugins.OnPreviewUpdate = previewUpdate;

		int initTracker = Plugins.ULS_UnityTrackerInit();
		if (initTracker < 0) {
			Debug.Log ("Failed to initialize tracker.");
		} else {
			Debug.Log ("Tracker initialization succeeded");
		}
	}
		
	void initCameraTexture (Texture preview, int rotate) {
		int w = preview.width;
		int h = preview.height;
		GetComponent<Renderer> ().material.mainTexture = preview;

		// adjust scale and position to map tracker points
#if UNITY_STANDALONE || UNITY_EDITOR
		// adjust scale and position to map tracker points
		transform.localScale = new Vector3 (w, -h, 1);
		transform.localPosition = new Vector3 (w/2, h/2, 1);
		transform.parent.localScale = new Vector3 (-1, -1, 1);
		transform.parent.localPosition = new Vector3 (w/2, h/2, 0);
		Camera.main.orthographicSize = h / 2;
#elif UNITY_IOS || UNITY_ANDROID
		int orthographicSize = w / 2;
		if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight) {
			orthographicSize = h / 2;
		}

		transform.localScale = new Vector3 (w, h, 1);
		transform.localPosition = new Vector3 (w/2, h/2, 1);//anchor: left-bottom
		transform.parent.localPosition = Vector3.zero; //reset position for rotation
		transform.parent.transform.eulerAngles = new Vector3 (0, 0, rotate);//orientation
		transform.parent.localPosition = transform.parent.transform.TransformPoint(-transform.localPosition); //move to center
		Camera.main.orthographicSize = orthographicSize;
#endif

		initDone = true;
	}

	void Update () {
		if (!initDone)	
			return;

        // Show tracking result
        if (0 < Plugins.ULS_UnityGetPoints(_trackPoints)) {

            _vertices[7] = new Vector3(_trackPoints[0], _trackPoints[1], 0);//0
            _vertices[8] = new Vector3(_trackPoints[2], _trackPoints[3], 0);//1
            _vertices[10] = new Vector3(_trackPoints[4], _trackPoints[5], 0);//2
            _vertices[28] = new Vector3(_trackPoints[6], _trackPoints[7], 0);//3
            _vertices[27] = new Vector3(_trackPoints[8], _trackPoints[9], 0);//4
            _vertices[16] = new Vector3(_trackPoints[10], _trackPoints[11], 0);//5
            _vertices[18] = new Vector3(_trackPoints[12], _trackPoints[13], 0);//6
            _vertices[33] = new Vector3(_trackPoints[14], _trackPoints[15], 0);//7
            _vertices[31] = new Vector3(_trackPoints[16], _trackPoints[17], 0);//8
            _vertices[13] = new Vector3(_trackPoints[18], _trackPoints[19], 0);//9
            _vertices[11] = new Vector3(_trackPoints[20], _trackPoints[21], 0);//10
            _vertices[6] = new Vector3(_trackPoints[22], _trackPoints[23], 0);//11
            _vertices[5] = new Vector3(_trackPoints[24], _trackPoints[25], 0);//12
            _vertices[26] = new Vector3(_trackPoints[26], _trackPoints[27], 0);//13
            _vertices[15] = new Vector3(_trackPoints[28], _trackPoints[29], 0);//14
            _vertices[17] = new Vector3(_trackPoints[30], _trackPoints[31], 0);//15
            _vertices[12] = new Vector3(_trackPoints[32], _trackPoints[33], 0);//16
            _vertices[14] = new Vector3(_trackPoints[34], _trackPoints[35], 0);//17
            _vertices[22] = new Vector3(_trackPoints[36], _trackPoints[37], 0);//18
            _vertices[32] = new Vector3(_trackPoints[38], _trackPoints[39], 0);//19
            _vertices[30] = new Vector3(_trackPoints[40], _trackPoints[41], 0);//20
            _vertices[29] = new Vector3(_trackPoints[42], _trackPoints[43], 0);//21
            _vertices[0] = new Vector3(_trackPoints[44], _trackPoints[45], 0);//22
            _vertices[4] = new Vector3(_trackPoints[46], _trackPoints[47], 0);//23
            _vertices[25] = new Vector3(_trackPoints[48], _trackPoints[49], 0);//24
            _vertices[24] = new Vector3(_trackPoints[50], _trackPoints[51], 0);//25
            _vertices[23] = new Vector3(_trackPoints[52], _trackPoints[53], 0);//26
            _vertices[2] = new Vector3(_trackPoints[54], _trackPoints[55], 0);//27
            _vertices[3] = new Vector3(_trackPoints[56], _trackPoints[57], 0);//28
            _vertices[1] = new Vector3(_trackPoints[58], _trackPoints[59], 0);//29
            _vertices[20] = (_vertices[33] + _vertices[18]) / 2;            
            Vector3 dir = (_vertices[13] - _vertices[20]).normalized;
            float dist = Vector3.Distance(_vertices[13], _vertices[20]);
            
            _vertices[21] = _vertices[20] - dir*1.8f* dist;
            _vertices[19] = (_vertices[16] + _vertices[21]) / 2;
            _vertices[34] = (_vertices[31] + _vertices[21]) / 2;
            _vertices[35] = (_vertices[31] + _vertices[21]) / 2;
            _vertices[9] = (_vertices[23] + _vertices[2]) / 2;
            // _vertices[10] = new Vector3(_trackPoints[62], _trackPoints[63], 0);//31
            // _vertices[10] = new Vector3(_trackPoints[64], _trackPoints[65], 0);//32
            // _vertices[10] = new Vector3(_trackPoints[66], _trackPoints[67], 0);//33
            // _vertices[10] = new Vector3(_trackPoints[68], _trackPoints[69], 0);//34
            // _vertices[10] = new Vector3(_trackPoints[70], _trackPoints[71], 0);//35


            /*
            for (int j = 0; j < Plugins.MAX_TRACKER_POINTS; ++j) {
                int v = j * 2;
                _vertices [j] = new Vector3 (_trackPoints [v], _trackPoints [v + 1], 0);
			}*/

            //_vertices[30] = new Vector3(_trackPoints[58], _trackPoints[59], 0);

            _mesh.MarkDynamic ();
			_mesh.vertices = _vertices;
			_mesh.RecalculateBounds ();
			faceMask.SetActive (true);

#if DRAW_MARKERS            
            //Plugins.MAX_TRACKER_POINTS - 6
            

            _marks[7].transform.localPosition = new Vector3(_trackPoints[0], _trackPoints[1], 0);//0
            _marks[8].transform.localPosition = new Vector3(_trackPoints[2], _trackPoints[3], 0);//1
            _marks[10].transform.localPosition = new Vector3(_trackPoints[4], _trackPoints[5], 0);//2
            _marks[28].transform.localPosition = new Vector3(_trackPoints[6], _trackPoints[7], 0);//3
            _marks[27].transform.localPosition = new Vector3(_trackPoints[8], _trackPoints[9], 0);//4
            _marks[16].transform.localPosition = new Vector3(_trackPoints[10], _trackPoints[11], 0);//5
            _marks[18].transform.localPosition = new Vector3(_trackPoints[12], _trackPoints[13], 0);//6
            _marks[33].transform.localPosition = new Vector3(_trackPoints[14], _trackPoints[15], 0);//7
            _marks[31].transform.localPosition = new Vector3(_trackPoints[16], _trackPoints[17], 0);//8
            _marks[13].transform.localPosition = new Vector3(_trackPoints[18], _trackPoints[19], 0);//9
            _marks[11].transform.localPosition = new Vector3(_trackPoints[20], _trackPoints[21], 0);//10
            _marks[6].transform.localPosition = new Vector3(_trackPoints[22], _trackPoints[23], 0);//11
            _marks[5].transform.localPosition = new Vector3(_trackPoints[24], _trackPoints[25], 0);//12
            _marks[26].transform.localPosition = new Vector3(_trackPoints[26], _trackPoints[27], 0);//13
            _marks[15].transform.localPosition = new Vector3(_trackPoints[28], _trackPoints[29], 0);//14
            _marks[17].transform.localPosition = new Vector3(_trackPoints[30], _trackPoints[31], 0);//15
            _marks[12].transform.localPosition = new Vector3(_trackPoints[32], _trackPoints[33], 0);//16
            _marks[14].transform.localPosition = new Vector3(_trackPoints[34], _trackPoints[35], 0);//17
            _marks[22].transform.localPosition = new Vector3(_trackPoints[36], _trackPoints[37], 0);//18
            _marks[32].transform.localPosition = new Vector3(_trackPoints[38], _trackPoints[39], 0);//19
            _marks[30].transform.localPosition = new Vector3(_trackPoints[40], _trackPoints[41], 0);//20
            _marks[29].transform.localPosition = new Vector3(_trackPoints[42], _trackPoints[43], 0);//21
            _marks[0].transform.localPosition = new Vector3(_trackPoints[44], _trackPoints[45], 0);//22
            _marks[4].transform.localPosition = new Vector3(_trackPoints[46], _trackPoints[47], 0);//23
            _marks[25].transform.localPosition = new Vector3(_trackPoints[48], _trackPoints[49], 0);//24
            _marks[24].transform.localPosition = new Vector3(_trackPoints[50], _trackPoints[51], 0);//25
            _marks[23].transform.localPosition = new Vector3(_trackPoints[52], _trackPoints[53], 0);//26
            _marks[2].transform.localPosition = new Vector3(_trackPoints[54], _trackPoints[55], 0);//27
            _marks[3].transform.localPosition = new Vector3(_trackPoints[56], _trackPoints[57], 0);//28
            _marks[1].transform.localPosition = new Vector3(_trackPoints[58], _trackPoints[59], 0);//29
            _marks[20].transform.localPosition = (_vertices[33] + _vertices[18]) / 2;
            Vector3 dir2 = (_vertices[13] - _vertices[20]).normalized;
            float dist2 = Vector3.Distance(_vertices[13], _vertices[20]);

            _marks[21].transform.localPosition = _vertices[20] - dir2 * 1.8f * dist2;
            _marks[19].transform.localPosition = (_vertices[16] + _vertices[21]) / 2;
            _marks[34].transform.localPosition = (_vertices[31] + _vertices[21]) / 2;
            _marks[35].transform.localPosition = (_vertices[31] + _vertices[21]) / 2;
            _marks[9].transform.localPosition = (_vertices[23] + _vertices[2]) / 2;

            for (int j = 0; j < Plugins.MAX_TRACKER_POINTS; ++j)
            {
                _marks[j].SetActive(drawMarkers);
            }
            /*for (int j=0;j< Plugins.MAX_TRACKER_POINTS; ++j) {
				int v = j*2;
				Vector3 pt = new Vector3(_trackPoints[v],_trackPoints[v+1], 0);
                
                _marks[j].transform.localPosition = (pt);
				//_marks[j].SetActive(drawMarkers);
			}*/
            //_marks[30].transform.localPosition = new Vector3(_trackPoints[58], _trackPoints[59], 0);
            //_marks[59].SetActive(drawMarkers);
#endif
        } else {
			faceMask.SetActive (false);
		}
	}

	bool frontal = true;
	bool flashLight = false;
	bool pause = false;
	bool enableTracker = true;

	void OnGUI() {
#if DRAW_MARKERS
		if (GUILayout.Button ("Show Markers", GUILayout.Height (80))) {
			drawMarkers ^= true;
		}
		GUILayout.Space (8);
#endif

		if (GUILayout.Button ("Next Mask", GUILayout.Height (80))) {
			maskTextureIndex = (maskTextureIndex + 1) % _masks.Length;
			faceMask.GetComponent<Renderer> ().material.mainTexture = _masks [maskTextureIndex];
		}

		GUILayout.Space (8);
		if (GUILayout.Button ("Toggle Tracker", GUILayout.Height (100))) {
			enableTracker ^= true;
			Plugins.ULS_UnityTrackerEnable (enableTracker);
		}

		GUILayout.Space (8);
		if (GUILayout.Button ("Switch Camera", GUILayout.Height (80))) {
			frontal = !frontal;
			if(frontal)
				Plugins.ULS_UnitySetupCamera (640, 480, 30, true);
			else
				Plugins.ULS_UnitySetupCamera (1280, 720, 60, false);
		}

		GUILayout.Space (8);
		if (GUILayout.Button ("Toogle FlashLight", GUILayout.Height (80))) {
			flashLight = !flashLight;
			Plugins.ULS_UnitySetFlashLight (flashLight);
		}

		GUILayout.Space (8);
		if (GUILayout.Button ("Change Scene", GUILayout.Height (80))) {
			Plugins.ULS_UnityTrackerTerminate ();
			SceneManager.LoadScene ("Object3D");
		}

		GUILayout.Space (8);
		if (GUILayout.Button ("Pause camera", GUILayout.Height (80))) {
			pause = !pause;
			Plugins.ULS_UnityPauseCamera (pause);
		}

		GUILayout.Label ("FlashLight:" + Plugins.ULS_UnityGetFlashLight ());
	}
}