﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class MarkVisualizer : MonoBehaviour {
    Text texto;
	// Use this for initialization
	void Start () {
        texto = transform.Find("Canvas").transform.Find("Text").gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setName(string name) {
        transform.Find("Canvas").transform.Find("Text").gameObject.GetComponent<Text>().text = name;
    }
}
